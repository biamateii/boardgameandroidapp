package com.halo.facebookfeed;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Pattern;

public class AddGame extends AppCompatActivity {

    ImageView imageView;
    Button addImageButton;
    EditText gameName;
    EditText description;
    EditText link;
    Uri uri;

    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_game);

        imageView = findViewById(R.id.newImg);
        addImageButton = findViewById(R.id.addImg);
        gameName = findViewById(R.id.gameNameText);
        description = findViewById(R.id.descriptionText);
        link = findViewById(R.id.linkText);

        addImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        requestPermissions(permissions, PERMISSION_CODE);
                    } else {
                        pickImageFromGallery();
                    }
                } else {
                    pickImageFromGallery();
                }
            }
        });
    }

    private void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    pickImageFromGallery();
                } else {
                    Toast.makeText(this, "Permission denied!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            imageView.setImageURI(data.getData());
            uri = data.getData();
        }
    }

    public void handleCancelClick(View view) {
        Intent intent = new Intent(this, Activity_Feed.class);
        startActivity(intent);
    }

    public void handleAddClick(View view) {
        if (gameName.getText().toString().isEmpty() || description.getText().toString().isEmpty() || link.getText().toString().isEmpty() || !(uri != null && uri != uri.EMPTY)) {
            Toast.makeText(getApplicationContext(), "Enter the Data!", Toast.LENGTH_SHORT).show();
        } else {

            if(link.getText().toString().matches("^(https?|ftp)://.*$")) {

                ModelFeed modelFeed = new ModelFeed(R.id.instructions, R.id.joinGroup, uri,
                        gameName.getText().toString(), description.getText().toString(), link.getText().toString());
                Activity_Feed.modelFeedArrayList.add(modelFeed);

                Activity_Feed.adapterFeed.notifyDataSetChanged();

                Toast.makeText(getApplicationContext(), "Game added!", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(this, Activity_Feed.class);
                startActivity(intent);
            }
            else
                Toast.makeText(getApplicationContext(), "Invalid Link!", Toast.LENGTH_SHORT).show();
        }
    }

}
