package com.halo.facebookfeed;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.halo.facebookfeed.ui.login.LoginActivity;

import java.util.ArrayList;

public class Activity_Feed extends AppCompatActivity {

    RecyclerView recyclerView;
    public static ArrayList<ModelFeed> modelFeedArrayList = new ArrayList<>();
    public static AdapterFeed adapterFeed;
    public static String gameName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapterFeed = new AdapterFeed(this, modelFeedArrayList);
        recyclerView.setAdapter(adapterFeed);

        //populateRecyclerView();
    }

    public void openInstructions(View view) {
        for (ModelFeed model : modelFeedArrayList) {

            if (model.getIdInstr() == R.id.instructions) {
                Uri uri = Uri.parse(model.getLink());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        }
    }

    public void joinGroup(View view) {
        for (ModelFeed model : modelFeedArrayList) {

            if (model.getIdGroup() == R.id.joinGroup) {
                gameName = model.getName();
                Intent intent = new Intent(this, Activity_Chat.class);
                startActivity(intent);
            }
        }
    }

    public void signOut(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void addGame(View view) {
        Intent intent = new Intent(this, AddGame.class);
        startActivity(intent);
    }

//    public void populateRecyclerView() {
//
//        ModelFeed modelFeed = new ModelFeed(1, new Bitmap(), R.drawable.img1,
//                "Monopoly", "Hei! Te plictisesti vienri seara si nu ai cu cine sa iesi? Hai si tu in grupul nostru sa facem o iesire in care sa jucam Monopoly.");
//        modelFeedArrayList.add(modelFeed);
//        modelFeed = new ModelFeed(2, R.drawable.img2,
//                "You're hired", "Cautam persoane interesate de acest joc, alatura-te si tu!");
//        modelFeedArrayList.add(modelFeed);
//        modelFeed = new ModelFeed(3, R.drawable.img3,
//                "Guess Who?", "Haha nu e vorba de acel cantaret, ci de un joc interactiv si foarte fain. Cautam persoane cu care ne putem juca sambataseara la un pahar de vin :)");
//        modelFeedArrayList.add(modelFeed);
//
//        adapterFeed.notifyDataSetChanged();
//    }
}
