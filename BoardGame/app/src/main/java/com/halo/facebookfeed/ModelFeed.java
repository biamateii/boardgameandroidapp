package com.halo.facebookfeed;

import android.graphics.Bitmap;
import android.net.Uri;

public class ModelFeed {

    int idInstr, idGroup;
    String name, status, link;
    Uri img;

    public ModelFeed(int idInstr, int idGroup, Uri img, String name, String status, String link) {
        this.idInstr = idInstr;
        this.idGroup = idGroup;
        this.name = name;
        this.status = status;
        this.img = img;
        this.link = link;
    }

    public Uri getImg() {
        return img;
    }

    public void setImg(Uri img) {
        this.img = img;
    }

    public int getIdInstr() {
        return idInstr;
    }

    public void setIdInstr(int idInstr) {
        this.idInstr = idInstr;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public String getLink() {
        return link;
    }

    public void setPostpic(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
